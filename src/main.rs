use clap::{Arg, Command};
use oxttl::TurtleParser;
use oxttl::TurtleSerializer;
use std::collections::HashMap;
use std::path::{Path, PathBuf};
use std::{env, fs};

fn main() {
    // get the CLI args
    let cli_matches = Command::new(env!("CARGO_PKG_NAME"))
        .author(env!("CARGO_PKG_AUTHORS"))
        .version(env!("CARGO_PKG_VERSION"))
        .about(env!("CARGO_PKG_DESCRIPTION"))
        .arg(
            Arg::new("input_dir_path")
                .value_parser(clap::value_parser!(PathBuf))
                .index(1)
                .required(true),
        )
        .arg(Arg::new("input_documents_glob").index(2).required(true))
        .arg(
            Arg::new("output_file_path")
                .value_parser(clap::value_parser!(PathBuf))
                .index(3)
                .required(true),
        )
        .get_matches();

    let input_dir_path: &PathBuf = cli_matches
        .get_one("input_dir_path")
        .expect("`input_dir_path`is required");
    let input_documents_glob_arg: &String = cli_matches
        .get_one("input_documents_glob")
        .expect("`input_documents_glob`is required");
    let input_documents_glob: glob::Pattern =
        glob::Pattern::new(input_documents_glob_arg.as_str()).expect("invalid `documents_glob`");
    let output_file_path: &PathBuf = cli_matches
        .get_one("output_file_path")
        .expect("`output_file_path`is required");

    merge_documents(input_dir_path, input_documents_glob, output_file_path);
}

fn merge_documents(
    input_dir_path: &Path,
    input_documents_glob: glob::Pattern,
    output_file_path: &Path,
) {
    // parse all the triples and extract all the prefixes
    let mut global_triples: Vec<oxrdf::Triple> = Vec::new();
    let mut global_prefixes: HashMap<String, String> = HashMap::new();

    let documents_glob =
        glob::Pattern::new(input_documents_glob.as_str()).expect("wrong glob pattern");
    let documents_path = input_dir_path.join(documents_glob.as_str());

    for document_path in glob::glob(documents_path.to_str().unwrap())
        .expect("Failed to read glob pattern")
        .filter_map(Result::ok)
    {
        parse_document(document_path, &mut global_triples, &mut global_prefixes);
    }

    // serialize all the triples
    let serialization_writer = Vec::new();
    let mut serializer = TurtleSerializer::new();
    for global_prefix in global_prefixes {
        serializer = serializer
            .with_prefix(global_prefix.0.as_str(), global_prefix.1.as_str())
            .expect("cannot add prefix to the serializer");
    }
    let mut configurated_serializer = serializer.clone().for_writer(serialization_writer);

    for global_triple in global_triples {
        configurated_serializer
            .serialize_triple(global_triple.as_ref())
            .expect("cannot serialize");
    }

    let result = configurated_serializer
        .finish()
        .expect("cannot finish the serialization");

    fs::write(output_file_path, &result).expect("Write file.");
}

fn parse_document(
    document_path: PathBuf,
    global_triples: &mut Vec<oxrdf::Triple>,
    global_prefixes: &mut HashMap<String, String>,
) {
    let file_as_bytes: Vec<u8> = fs::read(document_path).expect("cannot read file");

    let mut triples_reader = TurtleParser::new().for_slice(&file_as_bytes);

    triples_reader
        .next()
        .unwrap()
        .expect("cannot parse the first triple");

    let file_prefixes = triples_reader.prefixes().collect::<Vec<_>>();
    for file_prefix in file_prefixes {
        global_prefixes.insert(file_prefix.0.to_string(), file_prefix.1.to_string());
    }

    for file_triple in triples_reader {
        let triple = file_triple.unwrap();

        global_triples.push(triple);
    }
}

#[test]
pub fn test_1() {
    let input_dir_path =
        Path::new("/home/claudius/workspace/repositories/git/gitlab.rlp.net/adwmainz/nfdi4culture/cdmd/telemann-rdf/manifestations/");
    let input_documents_glob = glob::Pattern::new("**/*.ttl").expect("wrong glob pattern");
    let output_file_path =
    Path::new("/home/claudius/workspace/repositories/git/gitlab.rlp.net/adwmainz/nfdi4culture/cdmd/telemann-rdf/test.ttl");

    merge_documents(input_dir_path, input_documents_glob, output_file_path);
}
